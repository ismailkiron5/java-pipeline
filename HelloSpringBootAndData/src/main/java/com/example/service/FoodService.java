package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Food;
import com.example.repository.FoodDao;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class FoodService {
	
	private FoodDao foodDao;
	
	public Food getFoodByName(String name) {
		Food food = foodDao.findByFoodName(name);
		if(food == null) {
			return null;
		}
		return foodDao.findByFoodName(name);
	}
	
	public List<Food> getAllFood(){
		return foodDao.findAll();
	}
	
	public void insertFood(Food food) {
		foodDao.save(food); // the CrudRepository save method is really a saveOrUpdate, so if you want to update a record
		//you call teh save method again
	}
	
	public List<Food> getFoodByCalories(int cal){
		return foodDao.findByCalories(cal);
	}
	
	public Food getFoodByNameAndCal(String name, int cal) {
		return foodDao.findByFoodNameAndCalories(name, cal);
	}
	
	public void deleteFood(Food food) {
		foodDao.delete(food);
	}

}
